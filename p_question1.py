# Taking input from console.
def take_inputs():
    listOfDigits=[]
    try:
        for i in range(4):
            str = input()
            digits=[int(x) for x in str.split(',')]
            if not len(digits) == 2:
                raise
            listOfDigits.append(digits)
        two_dimensional_array(listOfDigits)
    except:
        print("Sorry, please provide valid integer pair value in the format x,y.")

# Creating two dimensional array and printing it.
def two_dimensional_array(listOfDigits):
    for i in range(len(listOfDigits)):
        rowNum = listOfDigits[i][0]
        colNum = listOfDigits[i][1]
        newlist = [[0 for col in range(colNum)] for row in range(rowNum)]
        val = 1
        for row in range(rowNum):
            for col in range(colNum):
                newlist[row][col]= val
                val +=  1
        print(newlist)

# calling input funtion.
take_inputs()