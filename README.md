# 2-D ARRAY

# Desciption:-

* program that takes 4 inputs, where each input consists of 2 numbers in the format x,y.
* And it prints the two-dimensional array having x rows and y columns for each input.

- Example
    * Suppose the following 4 inputs are given to the program:
    2,2
    2,3
    3,3
    3,4
    * Then, the output of the program will be like this:
    [[1, 2], [3, 4]]
    [[1, 2, 3], [4, 5, 6]]
    [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
    [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]

# Instruction to run:-
    - go to project directory using terminal.
    - Run `python3 p_question1.py.`